package homeworks.task2;

import java.util.Scanner;

public class Test {

    public static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Enter the size of the array: ");

        ImitationList myArray = new ImitationList(SCANNER);

        while (true) {
            menu();
            int key = SCANNER.nextInt();
            try {
                switch (key) {
                    case 1: {
                        System.out.println("Enter element that you want to add to the array, please:");

                        int element = SCANNER.nextInt();

                        myArray.addElement(element);

                        myArray.printArray();

                        break;
                    }
                    case 2: {
                        System.out.println("Enter the index of the item you want to change, please: ");

                        int indexOfElementsToChange = SCANNER.nextInt();

                        System.out.println("Enter the number: ");

                        int newElement = SCANNER.nextInt();

                        myArray.changeElementByIndex(indexOfElementsToChange, newElement);

                        myArray.printArray();

                        break;
                    }
                    case 3: {

                        System.out.println("Enter the number of elements, to which you want to increase the array, please: ");

                        int numberElementsToIncrease = SCANNER.nextInt();

                        myArray.increaseArray(numberElementsToIncrease);

                        myArray.printArray();

                        break;
                    }

                    case 4: {

                        System.out.println("Enter the number of elements, to which you want to reduce the array, please: ");

                        int numberElementsToReduce = SCANNER.nextInt();

                        myArray.reductionArray(numberElementsToReduce);

                        myArray.printArray();

                        break;
                    }


                    case 5: {

                        myArray.printArray();

                        break;
                    }

                    case 6: {

                        myArray.printArrayReverse();

                        break;
                    }

                    case 7: {

                        myArray.bubbleSort();

                        myArray.printArray();

                        break;
                    }

                    case 8: {

                        System.out.println("Enter the number of elements of the array that you want to add to the current array: ");

                        int numberElementsInNewArray = SCANNER.nextInt();

                        myArray.addArray(SCANNER, numberElementsInNewArray);

                        myArray.printArray();

                        break;
                    }


                    case 9: {

                        myArray.removeDuplicates();

                        myArray.printArray();

                        break;
                    }


                    case 10: {

                        System.out.println("Enter the index of the item you want to delete: ");

                        int removableItem = SCANNER.nextInt();

                        myArray.deleteElementByIndex(removableItem);

                        myArray.printArray();

                        break;
                    }


                    case 11: {

                        System.out.println("Fill array, please: ");

                        myArray.fillArray(SCANNER);

                        myArray.printArray();

                        break;
                    }

                    case 12:
                        return;
                    default:
                        return;
                }
            } catch (Exception e) {
                System.out.println("Exception" + e);
            }
        }
    }

    public static void menu() {
        System.out.println();
        System.out.println(
                "1) Add element.\n" +//++
                "2) Change element by index\n" +//++
                "3) Increase array\n" +//++
                "4) Reduction array\n" +//++
                "5) Print array in right order\n" +//++
                "6) Print array in reverse order\n" +//++
                "7) Bubble sort\n" +//++
                "8) Add array\n" +//++
                "9) Remove duplicates\n" +//++
                "10) Delete element by index\n" +//++
                "11) Fill array\n" +//++
                "12) Exit.");
    }
}


//    public static void main(String[] args) {
//        System.out.println("Enter number of arrays");
//        ImitationList myArray = new ImitationList(SCANNER);
//        myArray.fillArray(SCANNER);
//        myArray.addElement(SCANNER);
//        myArray.increaseArray(SCANNER);
//        myArray.printArray();
//        myArray.bubbleSort();
//        myArray.reductionArray(SCANNER);
//        myArray.changeElementByIndex(SCANNER);
//        myArray.deleteElementByIndex(SCANNER);
//        myArray.addArray(SCANNER);
//        myArray.removeDuplicates();
//        myArray.printArray();
//        myArray.printArrayReverse();
